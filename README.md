[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Juegos para el curso 2019-20
## JuegoTablero

Los juegos que se implementarán en esta práctica son los siguientes: 

 1. [Quatro!](https://drive.google.com/file/d/0B5KQ_9DlCmReakFKQ3hPZV85Vnc/view?usp=sharing). 
 2. [Quoridor](https://drive.google.com/file/d/0B5KQ_9DlCmRec3hYVXN1Y2xnVFE/view?usp=sharing).

Hay que repasar con detenimiento las reglas de los diferentes juegos. Para la práctica solo se utilizarán las reglas básicas de ambos juegos:

- Las partidas serán exclusivamente entre dos jugadores.
- Para **Quatro!** consideraremos alineamientos en horizontal, vertical o diagonal de una característica. Es decir, la opción más sencilla de las posibles.

## Agentes para la práctica

Para el diseño de los juegos se tendrán en cuenta los siguientes agentes especializados con sus tareas principales:

### AgenteMonitor

Este agente se encargará de coordinar la realización de todos los juegos posibles y la presentación de los resultados de esos juegos. Sus tareas principales serán:

- Localizar a los jugadores y organizadores para los diferentes juegos.
- Organizar a los jugadores que formarán parte de un juego.
- Localizar a un organizador que complete la realización de un juego.
- Recoger los resultados de los diferentes juegos que se estén organizando.

### AgenteJugador

Es el agente que adopta del **rol** de jugador para uno de los juegos disponibles, es decir, solo sabrá jugar a un único juego. El jugador debe diseñarse para seguir correctamente las reglas del juego y será responsable de mantener un estado correcto del juego, es decir, no tiene permitido **hacer trampas**. Este agente actúa de forma autónoma, esto es, no requiere supervisión de un usuario humano. Sus tareas principales serán:

- Aceptar la participación en un juego que le proponga un **AgenteMonitor**. Al menos debe aceptar jugar 3 juegos simultáneos.
- Realizar los turnos de juego que le sean solicitados para una partida perteneciente a uno de sus juegos activos.
- Recabar información del resultado de las partidas que esté jugando. De esta forma puede decidir si la partida ha concluido o no. Es necesario para conocer el número de juegos activos en los que está participando el jugador.

### AgenteOrganizador

Este es el agente encargado de organizar un juego específico para uno de los dos tipos de juego disponibles. Es decir, solo sabe organizar partidas para **Quatro!** o **Quoridor**. Sus tareas principales serán:

- Aceptar la organización de un juego propuesto por un **AgenteMonitor**. Al menos debe aceptar organizar 3 juegos simultáneos.
- Generar las partidas necesarias para los juegos que esté organizando.
- Localizar a **AgenteTablero** a los que pueda proponer realizar una partida.
- Obtener los resultados de las partidas para poder completar el juego al que corresponden.
- Informar del resultado del juego al **AgenteMonitor** que le solicitó su organización.

### AgenteTablero

Este es el agente es el encargado de organizar una partida para uno de los dos tipos de juego disponibles. Es decir, solo sabe organizar partidas para **Quatro!** o **Quoridor**. Sus tareas principales serán:

- Aceptar la organización de una partida propuesta por un **AgenteOrganizador**. Al menos debe aceptar organizar 3 partidas simultáneas.
- Organizar los turnos de juego necesarios para completar una partida.
- Comunicar el resultado de una partida a los agentes implicados, es decir, a los jugadores de la partida y al **AgenteOrganizador** que solicitó su realización.
- Mantener una lista con las partidas que ha organizado para que pueda solicitarse su reproducción por parte de un usuario humano.
	- Deberá almacenarse en disco los datos necesarios para que se pueda reproducir la partida.
	- Las partidas que se pueden reproducir serán todas las que ha completado el agente en las diferentes ejecuciones que haya tenido.
	- No requiere comunicación con los agentes implicados en la partida, solo se muestran los movimientos que ya se realizaron en la partida y el resultado que se obtuvo.

## Organización para la práctica

Como punto de partida para la práctica hay que descargarse el [proyecto](https://gitlab.com/ssmmaa/curso2019-20/juegotablero/-/archive/v0.1/juegotablero-v0.1.zip) para crear vuestro repositorio local. En ILIAS se creará una tarea con las instrucciones necesarias para la realización y entrega de la práctica.

El profesor se encargará del desarrollo del **AgenteMonitor**, el resto de los agentes serán responsabilidad de los alumnos. Cada uno de los miembros del equipo deberá hacerse responsable de una tarea para completar los agentes de la práctica. El responsable de la tarea es el encargado de crear todos los elementos necesarios de la misma. Todos los miembros de un equipo pueden cooperar para el desarrollo de las tareas, pero cada uno de ellos obtendrá la puntuación correspondiente a su tarea.

### Equipos de dos personas

El equipo elige uno de los juegos, es decir, o **Quoridor** o **Quatro!** para la realización de sus agentes. Las tareas que deberá completar el equipo serán:

1. **AgenteTablero**. Uno de los miembros del equipo se encarga del desarrollo completo de este agente. Deberá generarse un proyecto independiente en el repositorio del grupo que incluirá tanto la documentación e implementación para este agente.
2. **AgenteOrganizador** y **AgenteJugador**. Uno de los miembros del equipo se encarga del desarrollo completo de este agente. Deberá generarse un proyecto independiente en el repositorio del grupo que incluirá tanto la documentación e implementación para este agente.

### Equipos de tres personas

El equipo elige uno de los juegos, es decir, o **Quoridor** o **Quatro!** como juego principal en el que se desarrollan los agentes. Las tareas que deberá completar el equipo serán: 

1. **AgenteTablero**. Uno de los miembros del equipo se encarga del desarrollo completo de este agente. Deberá generarse un proyecto independiente en el repositorio del grupo que incluirá tanto la documentación e implementación para este agente.
2. **AgenteOrganizador** y **AgenteJugador**. Uno de los miembros del equipo se encarga del desarrollo completo de este agente. Deberá generarse un proyecto independiente en el repositorio del grupo que incluirá tanto la documentación e implementación para este agente.
3. **AgenteTablero**. Para el otro juego que no se ha elegido como principal por parte del equipo. Uno de los miembros del equipo se encarga del desarrollo completo de este agente. Deberá generarse un proyecto independiente en el repositorio del grupo que incluirá tanto la documentación e implementación para este agente.

### Equipos de cuatro personas

El equipo tiene que desarrollar los agentes para los dos juegos **Quoridor** y **Quatro!**. Las tareas que deberá completar el equipo serán: 

1. **AgenteTablero**. Para el juego de **Quoridor**. Uno de los miembros del equipo se encarga del desarrollo completo de este agente. Deberá generarse un proyecto independiente en el repositorio del grupo que incluirá tanto la documentación e implementación para este agente.
2. **AgenteOrganizador** y **AgenteJugador**.  Para el juego de **Quoridor**. Uno de los miembros del equipo se encarga del desarrollo completo de este agente. Deberá generarse un proyecto independiente en el repositorio del grupo que incluirá tanto la documentación e implementación para este agente.
3. **AgenteTablero**. Para el juego de **Quatro!**. Uno de los miembros del equipo se encarga del desarrollo completo de este agente. Deberá generarse un proyecto independiente en el repositorio del grupo que incluirá tanto la documentación e implementación para este agente.
4. **AgenteOrganizador** y **AgenteJugador**. Para el juego de **Quatro!**. Uno de los miembros del equipo se encarga del desarrollo completo de este agente. Deberá generarse un proyecto independiente en el repositorio del grupo que incluirá tanto la documentación e implementación para este agente.
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTk2NjE5ODk1NywtODA0NjI2NjE3LC0xOT
YxODgwMzEzXX0=
-->