/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.juegotablero.tareas;

import es.uja.ssmmaa.curso1920.ontologia.Vocabulario.TipoJuego;
import es.uja.ssmmaa.curso1920.ontologia.Vocabulario.TipoServicio;
import jade.core.AID;

/**
 * Comportamiento necesario para que un agente pueda añadir y eliminar agentes
 * que coincidan con la suscripción establecida en el servicio de páginas
 * amarillas.
 * @author pedroj
 */
public interface SubscripcionDF {
    public void addAgent(AID agente, TipoJuego juego, TipoServicio tipoServicio);
    public boolean removeAgent(AID agente, TipoJuego juego, TipoServicio tipoServicio);
}
