/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.juegotablero;

import java.util.Random;

/**
 *
 * @author pedroj
 */
public interface Constantes {
    // Generador de números aleatorios
    public final Random aleatorio = new Random();
    
    // Constantes
    public static final long TIME_OUT = 4000; // 4segundos
    public static final int FINALIZACION = 3; // segundos
    public static final String PREFIJO_ID = "Dyy"; // Formato fecha para el idJuego
    public static final int VACIO = 0;
    public static final int[] MIN_JUGADORES = {2,4,4};
    public static final int[] MAX_JUGADORES = {2,16,8};
    public static final int PRIMERO = 0;
    public static final int MENOR = -1;
    public static final int IGUAL = 0;
    public static final int MAYOR = 1;
    public final int DIA = 1;
    public final int NUM_JUEGO = 2;
    public static final String DIRECTORIO = "./data/";
    public static final String ARCHIVO = "juegos.dat";
    public static final String DIVISOR = "-";
}
