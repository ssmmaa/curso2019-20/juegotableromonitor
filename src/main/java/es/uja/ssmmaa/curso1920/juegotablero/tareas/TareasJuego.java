/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.juegotablero.tareas;

import es.uja.ssmmaa.curso1920.juegotablero.util.RegistroJuego;
import es.uja.ssmmaa.curso1920.ontologia.Vocabulario.TipoJuego;
import es.uja.ssmmaa.curso1920.ontologia.juegoTablero.Juego;
import es.uja.ssmmaa.curso1920.ontologia.juegoTablero.Organizador;
import jade.content.ContentManager;
import jade.content.onto.Ontology;

/**
 *
 * @author pedroj
 */
public interface TareasJuego {
    public ContentManager getManager(TipoJuego tipoJuego);
    public ContentManager getManager(String nameOntology);
    public Ontology getOntology(TipoJuego tipoJuego);
    public Ontology getOntology(String nameOntology);
    public void addMsgConsola(String msg);
    public void setRegistroJuego(String idJuego, RegistroJuego registro);
    public RegistroJuego getRegistroJuego(String idJuego);
    public void addJuego(Juego juego, Organizador agenteOrganizador);
}
